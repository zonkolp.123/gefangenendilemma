﻿using System;
using System.Collections.Generic;
using Gefangenendilemma.Basis;

namespace Gefangenendilemma
{
    /// <summary>
    /// Diese Klasse können Sie beliebig umschreiben, jenachdem welche Tasks sie erledigen.
    /// </summary>
    class VerwaltungProgramm
    {
        /// <summary>
        /// Diese Liste(Collection) enthält alle Gefangene/Strategien
        /// </summary>
        private static List<BasisStrategie> _strategien;
        
        static void Main(string[] args)
        {
            //bekannt machen der ganzen strategien
            _strategien = new List<BasisStrategie>();
            _strategien.Add(new GrollStrategie());
            _strategien.Add(new VerrateImmerStrategie());
            _strategien.Add(new Strategie1());
            _strategien.Add(new Strategie2());
            _strategien.Add(new Strategie3());
            
            string eingabe;
            do
            {
                // Begrüßung
                Console.Write("Willkommen zum Gefangenendilemma\n");
                Console.Write("0 - Verhör zwischen 2 Strategien\n");
                Console.Write("1 - Spiele selbst\n");
                Console.Write("X - Beenden\n");
                
                // Eingabe
                Console.Write("Treffen Sie ihre Option: ");
                eingabe = Console.ReadLine();

                // Auswerten der Eingabe
                switch (eingabe.ToLower())
                {
                    case "0":
                        Gefangene2();
                        break;
                    
                    case "1":
                        SpielerMenue();
                        break;
                    case "X":
                        break;
                    default:
                        Console.WriteLine($"Eingabe {eingabe} nicht erkannt.");
                        break;
                }
            } while (!"x".Equals(eingabe?.ToLower()));
        }

        /// <summary>
        /// Fragt 2 Strategien, Länge und Schwere ab.
        /// </summary>
        static void Gefangene2()
        {
            int st1, st2;
            int runde, schwere;
            
            Console.WriteLine("Willkommen zum Verhör zwischen 2 Strategien");
            for (int i = 0; i < _strategien.Count; i++)
            {
                Console.WriteLine($"{i} - {_strategien[i].Name()}");
            }
            Console.WriteLine("Wählen Sie ihre 2 Strategien:");
            st1 = VerwaltungKram.EingabeZahlMinMax("Wählen Sie die 1. Strategie", 0, _strategien.Count);
            st2 = VerwaltungKram.EingabeZahlMinMax("Wählen Sie die 2. Strategie", 0, _strategien.Count);
            runde = VerwaltungKram.EingabeZahlMinMax("Wie viele Runden sollen diese verhört werden?", 1, 101);
            schwere = VerwaltungKram.EingabeZahlMinMax("Wie schwer sind die Verstöße? (2=schwer)", 0, 3);

            // Auswahl treffen
            Console.Write("Soll jede Runde einzeln ausgewertet werden?\n");
            Console.Write("J - Ja\n");
            Console.Write("N - Nein\n");

            // Eingabe
            Console.Write("Treffen Sie ihre Option: ");
            string eingabe = Console.ReadLine();

            // Auswerten der Eingabe
            switch (eingabe.ToLower()) {
                case "j":
                    VerhoerSteps(st1, st2, runde, schwere);
                    break;
                case "n":
                    Verhoer(st1, st2, runde, schwere);
                    break;
                default:
                    Console.WriteLine($"Eingabe {eingabe} nicht erkannt.");
                    break;
            }
        }


        static void SpielerMenue()
        {
            int strategy;
            int rundenAnzahl, schwere;
            
            Console.WriteLine("Gegen welche KI willst du spielen? \n");
            for (int i = 0; i < _strategien.Count; i++)
            {
                Console.WriteLine($"{i} - {_strategien[i].Name()}");
            }
            
            strategy = VerwaltungKram.EingabeZahlMinMax("Wähle deinen Gegner:", 0, _strategien.Count);
            rundenAnzahl = VerwaltungKram.EingabeZahlMinMax("Wie viele Runden sollt ihr verhört werden?", 1, 100);
            schwere = VerwaltungKram.EingabeZahlMinMax("Wie schwer sind die Verstöße? (2=schwer)", 0, 3);
            
            SpielerVerhör(strategy,rundenAnzahl,schwere);
        }

        static void SpielerVerhör(int strategie, int runde, int schwere)
        {
            //holt die Strategie aus der Collection.
            BasisStrategie ki = _strategien[strategie];


            //setzt Startwerte
            int kiChoice = BasisStrategie.NochNichtVerhoert;
            int playerChoice = BasisStrategie.NochNichtVerhoert;
            
            int kiPoints = 0, playerPoints = 0;

            //beide Strategien über den Start informieren (Also es wird die Startmethode aufgerufen)
            ki.Start(runde, schwere);

            Console.WriteLine($"Verhör zwischen {ki.Name()} und dir für {runde} Runden.");

            string eingabe;
            
            for (int i = 0; i < runde; i++)
            {
                playerquest:
                Console.WriteLine( i + ". Runde - Treffe deine Wahl(0 = KOOPERATION | 1 = VERRAT): ");
                eingabe = Console.ReadLine();

                //beide verhören
                int kiReaktion = ki.Verhoer(playerChoice);
                int playerReaktion;
                try
                {
                    playerReaktion = int.Parse(eingabe);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bitte gebe ein Zahl ein!!!");
                    goto playerquest;
                }

                //Rückmeldung der KI
                String kiReactionString = "";
                if (kiReaktion==0)
                {
                    kiReactionString = " dicht gehalten. (KOOPERATION)";
                }
                else
                {
                    kiReactionString = " dich verraten. (VERRAT)";
                }

                int oldKiPoints = kiPoints;
                int oldPlayerPoints = playerPoints;

                Console.WriteLine(ki.Name() + " hat " + kiReactionString + "\n");


                //punkte berechnen
                Verhoeren(kiReaktion, playerReaktion, ref kiPoints, ref playerPoints,schwere);

                Console.WriteLine("PUNKTE \n  DU: +" + (playerPoints - oldPlayerPoints) +
                                         "\n  KI: +" + (kiPoints - oldKiPoints));
                
                
                //reaktion für den nächsten durchlauf merken
                kiChoice = kiReaktion;
                playerChoice = playerReaktion;

            }
            
            //ausgabe
            Console.WriteLine($"\n\n\n\n{ki.Name()} hat {kiPoints} Punkte erhalten.");
            Console.WriteLine($"Du hast du {playerPoints} Punkte erhalten.");
            if (kiPoints < playerPoints)
            {
                Console.WriteLine("Somit hat {0} gewonnen.", ki.Name());
            } 
            else if(kiPoints == playerPoints)
            {
                Console.WriteLine("Somit ist es ein Unentschieden.");
            }
            else
            {
                Console.WriteLine("Somit hast du gewonnen.");
            }
            Console.WriteLine("\n\n\n\n\n\n");
        }

        /// <summary>
        /// Startet ein Verhör zwischen der Strategie an der Position st1 und Position st2 über die Länge von runde und der Schwere schwere
        /// </summary>
        /// <param name="st1"></param>
        /// <param name="st2"></param>
        /// <param name="runde"></param>
        /// <param name="schwere"></param>
        static void Verhoer(int st1, int st2, int runde, int schwere)
        {
            //holt die beiden Strategien aus der Collection.
            BasisStrategie strategie1 = _strategien[st1];
            BasisStrategie strategie2 = _strategien[st2];

            //setzt Startwerte
            int reaktion1 = BasisStrategie.NochNichtVerhoert;
            int reaktion2 = BasisStrategie.NochNichtVerhoert;
            int punkte1 = 0, punkte2 = 0;

            //beide Strategien über den Start informieren (Also es wird die Startmethode aufgerufen)
            strategie1.Start(runde, schwere);
            strategie2.Start(runde, schwere);
            
            Console.WriteLine($"Verhör zwischen {strategie1.Name()} und {strategie2.Name()} für {runde} Runden.");
            
            //start
            for (int i = 0; i < runde; i++)
            {

                //beide verhören
                int aktReaktion1 = strategie1.Verhoer(reaktion2);
                int aktReaktion2 = strategie2.Verhoer(reaktion1);

                //punkte berechnen
               Verhoeren(aktReaktion1, aktReaktion2, ref punkte1, ref punkte2,schwere);
                
                //reaktion für den nächsten durchlauf merken
                reaktion1 = aktReaktion1;
                reaktion2 = aktReaktion2;
            }

            //ausgabe
            Console.WriteLine($"{strategie1.Name()} hat {punkte1} Punkte erhalten.");
            Console.WriteLine($"{strategie2.Name()} hat {punkte2} Punkte erhalten.");
            if (punkte1 < punkte2)
            {
                Console.WriteLine("Somit hat {0} gewonnen.", strategie1.Name());
            } 
            else
            {
                Console.WriteLine("Somit hat {0} gewonnen.", strategie2.Name());
            }
            
        }

        static void VerhoerSteps(int st1, int st2, int runde, int schwere) {
            //holt die beiden Strategien aus der Collection.
            BasisStrategie strategie1 = _strategien[st1];
            BasisStrategie strategie2 = _strategien[st2];

            //setzt Startwerte
            int reaktion1 = BasisStrategie.NochNichtVerhoert;
            int reaktion2 = BasisStrategie.NochNichtVerhoert;
            int punkte1 = 0, punkte2 = 0;
            string sAktReaktion1 = "";
            string sAktReaktion2 = "";

            //beide Strategien über den Start informieren (Also es wird die Startmethode aufgerufen)
            strategie1.Start(runde, schwere);
            strategie2.Start(runde, schwere);
            Console.WriteLine("");
            Console.WriteLine($"Verhör zwischen {strategie1.Name()} und {strategie2.Name()} für {runde} Runden.");

            //start
            for (int i = 0; i < runde; i++) {

                //beide verhören
                int aktReaktion1 = strategie1.Verhoer(reaktion2);
                int aktReaktion2 = strategie2.Verhoer(reaktion1);

                //Auswertung der Reaktion
                switch (aktReaktion1) {
                    case 0:
                        sAktReaktion1 = "Kooperieren";
                        break;
                    case 1:
                        sAktReaktion1 = "Verrat";
                        break;
                }

                switch (aktReaktion2) {
                    case 0:
                        sAktReaktion2 = "Kooperieren";
                        break;
                    case 1:
                        sAktReaktion2 = "Verrat";
                        break;
                }

                Console.WriteLine($"In Runde {i+1} entscheidet sich {strategie1.Name()} für {sAktReaktion1} und {strategie2.Name()} für {sAktReaktion2}");

                //punkte berechnen
                Verhoeren(aktReaktion1, aktReaktion2, ref punkte1, ref punkte2, schwere);

                Console.WriteLine($"{strategie1.Name()} hat jetzt {punkte1} Punkte.");
                Console.WriteLine($"{strategie2.Name()} hat jetzt {punkte2} Punkte.\r\n");

                //reaktion für den nächsten durchlauf merken
                reaktion1 = aktReaktion1;
                reaktion2 = aktReaktion2;
            }

            //ausgabe
            Console.WriteLine($"{strategie1.Name()} hat {punkte1} Punkte erhalten.");
            Console.WriteLine($"{strategie2.Name()} hat {punkte2} Punkte erhalten.");
            if (punkte1 < punkte2) {
                Console.WriteLine("Somit hat {0} gewonnen.", strategie1.Name());
            } else {
                Console.WriteLine("Somit hat {0} gewonnen.", strategie2.Name());
            }

        }

        /**
         * Ruft den richtigen Verhoer anhand der schwere auf.
         */
        private static void Verhoeren(int aktReaktion1, int aktReaktion2, ref int punkte1, ref int punkte2, int schwere)
        {
            switch (schwere)
            {
                case 0:
                    VerhoerLeichtPunkte(aktReaktion1,aktReaktion2,ref punkte1,ref punkte2);
                    break;
                case 1:
                    VerhoerMittelPunkte(aktReaktion1,aktReaktion2,ref punkte1,ref punkte2);
                    break;
                case 2:
                    VerhoerSchwerPunkte(aktReaktion1,aktReaktion2,ref punkte1,ref punkte2);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Berechnet für schwere Verstöße die Punkte und verwendet die 2 letzten Eingabeparameter als Rückgabe
        /// </summary>
        /// <param name="aktReaktion1"></param>
        /// <param name="aktReaktion2"></param>
        /// <param name="punkte1"></param>
        /// <param name="punkte2"></param>
        static void VerhoerSchwerPunkte(int aktReaktion1, int aktReaktion2, ref int punkte1, ref int punkte2)
        {
            if (aktReaktion1 == BasisStrategie.Kooperieren && aktReaktion2 == BasisStrategie.Kooperieren)
            {
                punkte1 += 4;
                punkte2 += 4;
                return;
            } 
            if (aktReaktion1 == BasisStrategie.Verrat && aktReaktion2 == BasisStrategie.Kooperieren)
            {
                punkte1 += 0;
                punkte2 += 10;
                return;
            }
            if (aktReaktion1 == BasisStrategie.Kooperieren && aktReaktion2 == BasisStrategie.Verrat)
            {
                punkte1 += 10;
                punkte2 += 0;
                return;
            }
            
            punkte1 += 8;
            punkte2 += 8;
            
        }
        
        /// <summary>
        /// Berechnet für mittlere Verstöße die Punkte und verwendet die 2 letzten Eingabeparameter als Rückgabe
        /// </summary>
        /// <param name="aktReaktion1"></param>
        /// <param name="aktReaktion2"></param>
        /// <param name="punkte1"></param>
        /// <param name="punkte2"></param>
        static void VerhoerMittelPunkte(int aktReaktion1, int aktReaktion2, ref int punkte1, ref int punkte2)
        {
            if (aktReaktion1 == BasisStrategie.Kooperieren && aktReaktion2 == BasisStrategie.Kooperieren)
            {
                punkte1 += 10;
                punkte2 += 10;
                return;
            } 
            if (aktReaktion1 == BasisStrategie.Verrat && aktReaktion2 == BasisStrategie.Kooperieren)
            {
                punkte1 += 8;
                punkte2 += 0;
                return;
            }
            if (aktReaktion1 == BasisStrategie.Kooperieren && aktReaktion2 == BasisStrategie.Verrat)
            {
                punkte1 += 0;
                punkte2 += 8;
                return;
            }
            
            punkte1 += 4;
            punkte2 += 4;
            
        }
        
        /// <summary>
        /// Berechnet für leichte Verstöße die Punkte und verwendet die 2 letzten Eingabeparameter als Rückgabe
        /// </summary>
        /// <param name="aktReaktion1"></param>
        /// <param name="aktReaktion2"></param>
        /// <param name="punkte1"></param>
        /// <param name="punkte2"></param>
        static void VerhoerLeichtPunkte(int aktReaktion1, int aktReaktion2, ref int punkte1, ref int punkte2)
        {
            if (aktReaktion1 == BasisStrategie.Kooperieren && aktReaktion2 == BasisStrategie.Kooperieren)
            {
                punkte1 += 3;
                punkte2 += 3;
                return;
            } 
            if (aktReaktion1 == BasisStrategie.Verrat && aktReaktion2 == BasisStrategie.Kooperieren)
            {
                punkte1 += 0;
                punkte2 += 9;
                return;
            }
            if (aktReaktion1 == BasisStrategie.Kooperieren && aktReaktion2 == BasisStrategie.Verrat)
            {
                punkte1 += 9;
                punkte2 += 0;
                return;
            }
            
            punkte1 += 6;
            punkte2 += 6;
            
        }
        
    }
}