using Gefangenendilemma.Basis;

namespace Gefangenendilemma
{
    /// <summary>
    /// Nur nutzen, wenn es ein 3. Gruppenmitglied gibt.
    /// </summary>
    public class Strategie3 : BasisStrategie
    {
        /**
         * Gibt den Namen der Strategie zurück, wichtig zum Anzeigen für die Auswahl
         */
        public override string Name()
        {
            return "Bitte anpassen";
        }

        /**
        *  Gibt den Namen des Autors der Strategie zurück, wichtig für die Turnierpart um den Sieger zu ermitteln.
        */
        public override string Autor()
        {
            return "Bitte anpassen";
        }

        /**
        *<summary>
        *    Teilt mit, dass ein Verhoer jetzt startet
        *</summary>
        *<param name="runde">Anzahl der Runden, die verhört wird</param>
        *<param name="schwere">Schwere des Verbrechen (VLeicht = 0, VMittel = 1, VSchwer = 2)</param>
        */
        public override void Start(int runde, int schwere)
        {
            //Vorbereitungen für Start
        }

        /**
        *<summary>
        *   Verhoert einen Gefangenen
        *</summary>
        *<param name="letzteReaktion">Reaktion des anderen Gefangenen, die Runde davor (NochNichtVerhoert = -1, Kooperieren = 0, Verrat = 1)</param>
        *<returns>Gibt die eigene Reaktion für diese Runde zurück (Kooperieren = 0, Verrat = 1)</returns>
        */
        public override int Verhoer(int letzteReaktion)
        {
            //Strategie hier ergänzen

            return Verrat;
        }
    }
}